# Modèle de données

Voici le modèle conceptuel de donnée pour les relevés Agrinichoirs :

```mermaid
classDiagram
    fermes "1" <-- "*" parcelles
    parcelles "1" <-- "*" nichoirs
    nichoirs "1" <-- "*" releves
    parcelles "1" <-- "*" releves

    class fermes {
      +entier id
      +texte nom_ferme
      +texte nom_exploitant
      +texte adresse
      +texte commune
      +texte code_commune
      +texte type_projet
      +texte commentaire
      +texte financement
    }

    class parcelles {
      +entier fid
      +texte nom_parcelle
      +texte culture
      +entier surface
      +texte pratique
      +date date_installation
      +texte presence_haie
      +blob wkb_geometry
      +entier ferme
    }

    class nichoirs {
      +entier fid
      +type type
      +texte modele
      +date date_installation
      +texte orientation
      +decimal hauteur
      +booleen dans_haie
      +texte commentaire
      +entier ref
      +entier z_index
      +blob wkb_geometry
      +entier parcelle
    }

    class releves {
      +entier fid
      +horodatage date
      +texte observateur
      +booleen occupe
      +booleen visite
      +decimal nb_occupant
      +entier nb_cadavre
      +blob wkb_geometry
      +texte type
      +entier nichoir
      +entier parcelle
      +entier annee
    }
```

Le type `blob` signifie **B**inary **L**arge **Ob**ject est un type de donnée permettant le stockage de données binaires (dans notre cas des données géographiques - Point, Ligne et Polygones).

Pour que les scripts de calculs fonctionnent il faut :
- que toutes ces tables soient présentes dans le fichier GPKG
- que tous les champs de toutes les tables soient également présents
- que les types de données des champs n'ait pas changés (ex: booléen)

ℹ️ il est possible d'ajouter des champs, mais il n'est pas possible de supprimer ou renommer des champs (ceci afin que les requêtes de calcul d'indicateurs fonctionnent correctement)

Si toutefois un renommage de champ devait avoir lieu,  il est possible d'adapter les scripts SQL de la section [Documentation des calculs Agrinichoirs](#documentation-des-calculs-agrinichoirs)

C'est souvent le cas lors d'import/export de données vers/depuis le GPKG où certains champs changent de nom. C'est tout particulièrement le cas du champ contenant les informations géométriques qui peut passer du nom `wkb_geometry` à `geom` ou `geometry` selon les outils de traitement de la donnée.

:warning: Certains type de champs sont également assez sensibles lors de multiples import/export de données vers/depuis le GPKG, c'est notamment le cas du type `booléen` et du type `date` et `horodatage` qui peuvent être transformés en type `texte`. Cela peut avoir des effets inattendu lors du calcul des indicateurs, voire même de rendre ce calcul impossible.

# Documentation des calculs Agrinichoirs

On utilise directement l'outil DB Manager de QGIS pour effectuer ces mises à jour de base de données (Menu **base de données** > **DB Manager**)

## Attribution d'un identifiant parcelle aux nichoirs

### Ajout d'une colonne _parcelle_ à la table _nichoirs_

Cette colonne contiendra une référence de d'identifiant de la parcelle

Ouvrir le **DB Manager** et exécuter la requête suivante sur le fichier `GPKG`

```sql
ALTER TABLE nichoirs ADD COLUMN parcelle integer REFERENCES parcelles (fid) ON DELETE NO ACTION ON UPDATE NO ACTION
```

Voici une version imagée de ce qu'il faut faire :

![procédure pour accéder au DB Manager et y exécuter une requête](resources/setup/db_manager.gif)

ℹ️ Cette requête SQL altère la table (`ALTER TABLE` [^alter-table]) puis ajoute une containte de clé étrangère (`REFERENCES` [^foreign-key]), c.a.d. que la base vérifie
que les identifiants de parcelle ajoutés dans la table _nichoirs_ sont bien présents
la table _parcelles_

### Recherche d'un identifiant de parcelle et mise à jour de la table  _nichoirs_

:warning: A cause de d'un bug [^gpkg-amphibious-mode], il faudra exécuter cette requête ci-dessous avant chaque requête
impliquant une requête spatiale (avec les fonctions commençant par _ST_)

```sql
SELECT EnableGpkgAmphibiousMode()
```

Copier l'ensemble de cette suite de requête dans **DB Manager** puis exécuter requête par requête
```sql
-- 1ère requête : Contournement du bug N°27914
SELECT EnableGpkgAmphibiousMode();

-- 2ème requête : création d'une table tempraire contenant les informations de
-- croisement entre les parcelles et les nichoirs
CREATE TABLE tmp AS
SELECT MAX(p.fid) as parcelle, n.fid
FROM parcelles p, nichoirs n
WHERE ST_Within(n.wkb_geometry, p.wkb_geometry)
GROUP BY n.fid;

-- 3ème requête : mise à jour des identifiants de parcelles sur la table nichoirs
UPDATE nichoirs
SET parcelle =(
  SELECT parcelle FROM tmp
  WHERE fid = nichoirs.fid
)

-- 4ème requête : supprimer la table tmp, désormais inutile
DROP TABLE IF EXISTS tmp;
```

Voici une version imagée de ce qu'il faut faire :

![procédure pour exécuter requête par requête](resources/setup/query_by_query.gif)

ℹ️ L'exécution de la requête spatial peut prendre un peu de temps, car les calculs spatiaux
sont très consommateurs en CPU.

### Vérification rapide

Pour faire une vérification rapide du résultat d'attribution d'un identifiant _parcelle_
aux _nichoirs_, exécuter la requête suivante :

```sql
SELECT p.nom_parcelle, count(n.fid) AS nb_nichoirs
FROM parcelles p
LEFT JOIN nichoirs n ON n.parcelle = p.fid
GROUP BY  p.nom_parcelle
```

### Recherche d'un identifiant de parcelle et mise à jour de la table  _releves_

Même principe que pour la table _nichoirs_, il s'agit ici d'attribuer un identifiant de
parcelle aux relevés en se basant sur leur position géographique.

### Ajout d'une colonne _parcelle_ à la table _releves_

```sql
ALTER TABLE releves ADD COLUMN parcelle integer REFERENCES parcelles (fid) ON DELETE NO ACTION ON UPDATE NO ACTION
```

### Recherche d'un identifiant de parcelle et mise à jour de la table  _releves_

```sql
-- 1ère requête : Contournement du bug N°27914
SELECT EnableGpkgAmphibiousMode();

-- 2ème requête : création d'une table tempraire contenant les informations de
-- croisement entre les parcelles et les nichoirs
CREATE TABLE tmp AS
SELECT MAX(p.fid) as parcelle, r.fid
FROM parcelles p, releves r
WHERE ST_Within(r.wkb_geometry, p.wkb_geometry)
GROUP BY r.fid;

-- 3ème requête : mise à jour des identifiants de parcelles sur la table nichoirs
UPDATE releves
SET parcelle =(
  SELECT parcelle FROM tmp
  WHERE fid = releves.fid
)

-- 4ème requête : supprimer la table tmp, désormais inutile
DROP TABLE IF EXISTS tmp;
```

### Vérification rapide

Pour faire une vérification rapide du résultat d'attribution d'un identifiant _parcelle_
aux _releves_, exécuter la requête suivante :

```sql
SELECT p.nom_parcelle, count(r.fid) AS nb_releves
FROM parcelles p
LEFT JOIN releves r ON r.parcelle = p.fid
GROUP BY p.nom_parcelle
```

## Calcul des indicateurs d'occupation de la parcelle

### Implémentation et calcul direct dans la base de données

Les indicateurs d'occupation sont calculés de façon séquentielle dans une vue SQL.

On calcule d'abord :

* le nombre de nichoirs par parcelle (cela correspond à la _Common Table Expression_ nommée **nichoirs_par_parcelle** dans la requête ci-dessous)
* le nombre de releves par parcelle par année (aka **releves_par_parcelle**)
* l'occupation par type de nichoirs par année (aka **occupation_par_type**)
* l'occupation par type de nichoirs par année précédente (aka **occupation_annee_precedente**)


Copier l'ensemble de cette suite de requête dans **DB Manager** puis exécuter requête par requête :
```sql
-- 1ère requête : nettoyage de la vue
DROP VIEW IF EXISTS vue_occupation_parcelles;

-- 2ème requête : calcul de l'occupation des parcelles
CREATE VIEW vue_occupation_parcelles AS
WITH nichoirs_par_parcelle AS (
	SELECT DISTINCT p.fid,
		n.type,
		count(n.fid) OVER (PARTITION BY n.parcelle, n.type) AS nb_nichoirs
	FROM nichoirs n
	LEFT JOIN parcelles p ON n.parcelle = p.fid
	WHERE p.fid IS NOT NULL
), releves_par_parcelle AS (
	SELECT DISTINCT p.fid,
		r.type,
		strftime('%Y',date) AS annee,
		count(r.fid) OVER (PARTITION BY r.parcelle, r.type, strftime('%Y',date)) AS nb_releves
	FROM releves r
	LEFT JOIN parcelles p ON r.parcelle = p.fid
	WHERE p.fid IS NOT NULL
), occupation_par_type AS (
	SELECT r.parcelle,
		strftime('%Y',date) AS annee,
		r.type,
		count(*) AS nb_nichoir_occupe
	FROM releves r
	WHERE r.occupe IS TRUE OR r.occupe = 'true'
	GROUP BY r.parcelle, (strftime('%Y',date)), r.type
), occupation_annee_precedente AS (
	SELECT occupation_par_type.parcelle,
		occupation_par_type.annee,
		occupation_par_type.type,
		occupation_par_type.nb_nichoir_occupe,
		lag(occupation_par_type.nb_nichoir_occupe, 1) OVER (PARTITION BY occupation_par_type.parcelle, occupation_par_type.type
			ORDER BY occupation_par_type.annee) AS nb_nichoir_occupe_annee_prec
	FROM occupation_par_type
)
	SELECT p.fid,
		p.nom_parcelle,
		rpp.type,
		rpp.annee,
		CASE WHEN occ.nb_nichoir_occupe IS NOT NULL
			THEN round((occ.nb_nichoir_occupe * 100 / rpp.nb_releves), 2) || ' %'
			ELSE '0 %'
		END AS taux_occupation,
		CASE WHEN occ.nb_nichoir_occupe IS NOT NULL
			THEN round((occ.nb_nichoir_occupe * 100 / npp.nb_nichoirs), 2) || ' %'
			ELSE '0 %'
		END AS taux_occupation_nichoirs,
		CASE WHEN occ.nb_nichoir_occupe IS NOT NULL
			THEN (round((occ.nb_nichoir_occupe * 1.0 - oap.nb_nichoir_occupe_annee_prec * 1.0) / oap.nb_nichoir_occupe_annee_prec * 1.0, 2) * 100) || ' %'
			ELSE (round((0 - oap.nb_nichoir_occupe_annee_prec * 1.0) / oap.nb_nichoir_occupe_annee_prec * 1.0, 2) * 100) || ' %'
		END AS evolution_occupation,
		round(rpp.nb_releves / p.surface) AS densite_releves,
		round(npp.nb_nichoirs / p.surface) AS densite_nichoirs,
		CASE WHEN occ.nb_nichoir_occupe IS NOT NULL
			THEN occ.nb_nichoir_occupe ELSE 0
		END AS nb_nichoir_occupe,
		CASE WHEN oap.nb_nichoir_occupe_annee_prec IS NOT NULL
			THEN oap.nb_nichoir_occupe_annee_prec ELSE 0
		END AS nb_nichoir_occupe_annee_precdedente,
		rpp.nb_releves
	FROM parcelles p
	LEFT JOIN releves_par_parcelle rpp ON p.fid = rpp.fid
	LEFT JOIN occupation_par_type occ ON rpp.fid = occ.parcelle AND rpp.type = occ.type AND occ.annee = rpp.annee
	LEFT JOIN nichoirs_par_parcelle npp ON npp.fid = occ.parcelle AND npp.type = occ.type
	LEFT JOIN occupation_annee_precedente oap ON npp.fid = oap.parcelle AND npp.type = oap.type AND occ.annee = oap.annee
	GROUP BY p.fid, p.nom_parcelle, rpp.type, rpp.annee, npp.nb_nichoirs, occ.nb_nichoir_occupe, oap.nb_nichoir_occupe_annee_prec, rpp.nb_releves
	ORDER BY p.nom_parcelle, rpp.type, rpp.annee DESC;
```

Une fois cette vue créé, il est possible de l'ajouter à `QGis` comme table attributaire et de faire une jointure avec la table _parcelles_ en utilisant les clés _fid_ pour la table _parcelles_ et _fid_ pour la table attributaire _vue_occupation_parcelles_

Il est également possible de visualiser le contenu de cette vue diretmeent dans le **DB Manager** en exécutant cette requête :

```sql
SELECT * FROM vue_occupation_parcelles
```

# Fusion des Géopackages

Deux possibilités actullement pour Agrinichoirs :

* via un script (_mettre en PJ_)
* via un utilitaire

## Utilitaire

DB Browser for SQLite =>  https://sqlitebrowser.org/blog/version-3-12-0-released/

1. Ouvrir un GPKG
2. Attacher un autre fichier GPKG en allant dans **fichier** > **Attacher une base de donnée**
3. Dans la stucture de la donnée on note la présence d'un sous-menu qui correspond à la base de données attachée (avec un suffixe qui lui est propre)

On peut aller réaliser des opérations sur les deux bases de données à la fois

ex:

```sql
select count(*) from nichoirs
select count(*) from mlb2_MP.nichoirs
```

ℹ️ Le suffixe `mlb2_MP` peut varier d'une base de donnée attachée à l'autre

On peut donc procédé à l'insertion des données d'une base à l'autre :


```sql
INSERT OR IGNORE INTO nichoirs
SELECT * FROM  mlb2_MP.nichoirs
```

`nichoirs` étant une table dans la base de donnée receptrice et `mlb2_MP.nichoirs` étant une table dans la base de donnée émettrice

ℹ️ L'instruction `OR IGNORE` permet d'éviter les collisions sur les contraintes de clés primaires et d'unicité

# Déduplication des données


La déduplication des données via les scripts SQL ci dessous ne supprime que les enregistrements strictement égaux

:warning: Selon la manière dont se déroule la fusion et selon les outils utilisés, il se peut que la colonne d'identification soit `fid` ou `ogc_fid`
Il faut donc adapter les requêtes ci-dessous selon les cas (en remplace toutes les occurences de `fid` par `ogc_fid` le cas échéant).

 ```sql
 --------------------------------------------------------------------------------
 -- Dedupe query for `parcelle` table
 --------------------------------------------------------------------------------

 WITH pure_equality_check AS (
 	SELECT FIRST_VALUE(fid) OVER(PARTITION BY wkb_geometry, nom_ferme, nom_parcelle, culture, surface, pratique, date_installation, presence_haie ORDER BY fid) as fid
 	FROM parcelles
 ), items_to_delete AS (
 	SELECT i.fid
 	FROM parcelles i
 	LEFT JOIN pure_equality_check pec ON pec.fid = i.fid
 	WHERE pec.fid IS NULL
 )
 DELETE FROM parcelles
 WHERE fid IN (SELECT fid FROM items_to_delete);

 --------------------------------------------------------------------------------
 -- Dedupe query for `nichoirs` table
 --------------------------------------------------------------------------------

 WITH pure_equality_check AS (
 	SELECT FIRST_VALUE(fid) OVER(PARTITION BY wkb_geometry, type, modele, date_installation, orientation, hauteur, dans_haie, commentaire ORDER BY fid) as fid
 	FROM nichoirs
 ), items_to_delete AS (
 	SELECT i.fid
 	FROM nichoirs i
 	LEFT JOIN pure_equality_check pec ON pec.fid = i.fid
 	WHERE pec.fid IS NULL
 )
 DELETE FROM nichoirs
 WHERE fid IN (SELECT fid FROM items_to_delete);

 --------------------------------------------------------------------------------
 -- Dedupe query for `releves` table
 --------------------------------------------------------------------------------

 WITH pure_equality_check AS (
 	SELECT FIRST_VALUE(fid) OVER(PARTITION BY wkb_geometry, date, observateur, type, occupe, visite, nb_occupant, nb_cadavre ORDER BY fid) as fid
 	FROM releves
 ), items_to_delete AS (
 	SELECT i.fid
 	FROM releves i
 	LEFT JOIN pure_equality_check pec ON pec.fid = i.fid
 	WHERE pec.fid IS NULL
 )
 DELETE FROM releves
 WHERE fid IN (SELECT fid FROM items_to_delete);
```




[^alter-table]: https://www.sqlite.org/lang_altertable.html
[^foreign-key]:https://www.sqlite.org/syntax/foreign-key-clause.html
[^gpkg-amphibious-mode]: https://github.com/qgis/QGIS/issues/27914
